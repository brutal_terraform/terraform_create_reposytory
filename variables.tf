variable "deploy_key" {
  description = "Local pub key"
  type = string
}
variable "gitlab_token" {
  description = "Gitlab token"
  type = string
}