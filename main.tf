terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.8.0"
    }
  }
}

provider "gitlab" {
    token = var.gitlab_token
	base_url = "https://gitlab.rebrainme.com/"
}
data "gitlab_group" "terraform" {
  group_id = 9299
}

resource "gitlab_project" "terraform" {
  name = "ter_01"
  description = "My awesome terraform"
  visibility_level = "private"
  namespace_id = data.gitlab_group.terraform.id
}

resource "gitlab_deploy_key" "terraform" {
  project = gitlab_project.terraform.id
  title   = "Example deploy key"
  key     = var.deploy_key
  can_push = "true"
  
}
